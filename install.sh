#! /bin/bash

CUR_PATH=`pwd`;
function createDir(){
	ps=(bin conf);
    for p in ${ps[@]}; do
        cf="$CUR_PATH/$p";
        if [[ ! -d $cf ]]; then
            mkdir $cf;
        fi
    done
}
function createConfiguration(){
	f="$CUR_PATH/src/configuration.conf";
	
	echo "APP_PATH='$CUR_PATH';" > $f;
	echo "BIN_PATH='$CUR_PATH/bin';" >> $f;
	echo "SRC_PATH='$CUR_PATH/src';" >> $f;
	echo "CONF_PATH='$CUR_PATH/conf';" >> $f;
	echo $f;

}

function createRunFile(){
	f="$CUR_PATH/bin/run.sh";
	echo "#! /bin/bash" > $f
	echo "source $1;" >> $f
	echo "source $SRC_PATH/web.sh;" >> $f
	echo "source $SRC_PATH/main.sh;" >> $f
	chmod 777 "$f";
	# echo $f;
}

function main(){
	clear;
	echo "Welcome to Etsoftware.";
	createDir;
	cnf=`createConfiguration`;
	source $cnf;
	createRunFile $cnf;
	echo "please add program to contab.";
	echo "crontab -e";
	echo "* */12 * * * $CUR_PATH/bin/run.sh";
	echo "You can configure your information in the conf directory.";
	echo "$CONF_PATH/";

}
main;